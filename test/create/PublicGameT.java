package create;

import create.game.Game;
import java.util.Arrays;
import java.util.Scanner;
import play.game.ActiveGame;
import play.game.Field;
import play.game.GameHandler;

/**
 * D
 *
 */
public class PublicGameT {

    public static void main(String[] args) {
        Game publicGame = new Game("Spieler 1");
        publicGame.init();
        ActiveGame game = publicGame.findAndStart((String msg) -> {
            System.out.println("Public game giving status: " + msg);
        });

        game.startGame(new GameHandler() {
            @Override
            public Field onTurn(Field field,int c) {
                System.out.println(Arrays.deepToString(field.fieldArray));
                System.out.println("please input x: ");
                Scanner scanner = new Scanner(System.in);
                int x = scanner.nextInt();
                System.out.println("please input y: ");
                int y = scanner.nextInt();
                System.out.println("x:" + x + "y:" + y);
                field.set(x, y, c);
                return field;
            }

            @Override
            public void onWait() {
                System.out.println("its not your turn its your enemys!");
            }

            @Override
            public void onEnd(Field field, String winnerUsername, int winner,int c) {
                if (winner == c) {
                    System.out.println("YOU WON!");
                } else {
                    System.out.println(winnerUsername + "WON!\nYou lost ;(");
                }
            }
        });

    }
}
