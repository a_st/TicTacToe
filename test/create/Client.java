package create;

import java.io.IOException;
import java.net.InetAddress;
import java.rmi.UnknownHostException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import play.game.ActiveGame;
import play.game.Field;
import play.game.GameHandler;

/**
 *
 * @author ar
 */
public class Client {

    public static void main(String[] args) throws InterruptedException {
//        GameFinder finder = new GameFinder();
//        finder.scanForGames((SortedSet<PublicGameEntry> entries) -> {
//            System.out.println(entries);
//        });
//        Thread.sleep(10000);
//        finder.stopScanning();
        try {
            ActiveGame game = GameJoiner.joinGame(InetAddress.getByName("192.168.2.111"), 13478, "Spieler 2");
            game.startGame(new GameHandler() {
                @Override
                public Field onTurn(Field field,int c) {
                    System.out.println(Arrays.deepToString(field.fieldArray));
                    System.out.println("please input x: ");
                    Scanner scanner = new Scanner(System.in);
                    int x = scanner.nextInt();
                    System.out.println("please input y: ");
                    int y = scanner.nextInt();
                    System.out.println("x:" + x + "y:" + y);
                    field.set(x, y, c);
                    return field;
                }

                @Override
                public void onWait() {
                    System.out.println("its not your turn its your enemys!");
                }

                @Override
                public void onEnd(Field field, String winnerUsername, int winner,int c) {
                    if (winner == c) {
                        System.out.println("YOU WON!");
                    } else {
                        System.out.println(winnerUsername + "WON!\nYou lost ;(");
                    }
                }
            });
        } catch (UnknownHostException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
