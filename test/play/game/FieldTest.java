package play.game;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ar
 */
public class FieldTest {

    public FieldTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getWinner method, of class Field.
     */
    @Test
    public void testGetWinner() {
        System.out.println("getWinner");
        Field field = new Field();

        //vertically
        field.fieldArray[0] = new int[]{1, 2, 0};
        field.fieldArray[1] = new int[]{1, 1, 1};
        field.fieldArray[2] = new int[]{2, 0, 0};

        int expResult = 1;
        int result = field.getWinner();
        assertEquals(expResult, result);
        //horizontally
        field.fieldArray[0] = new int[]{1, 2, 0};
        field.fieldArray[1] = new int[]{1, 2, 1};
        field.fieldArray[2] = new int[]{2, 2, 0};

        expResult = 2;
        result = field.getWinner();
        assertEquals(expResult, result);
        //diagonally
        field.fieldArray[0] = new int[]{2, 2, 1};
        field.fieldArray[1] = new int[]{1, 1, 0};
        field.fieldArray[2] = new int[]{1, 2, 1};

        expResult = 1;
        result = field.getWinner();
        assertEquals(expResult, result);
    }

}
