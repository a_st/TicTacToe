package play.game;

/**
 *
 * @author Aaron
 */
public abstract class GameHandler {
    
    /**
     *
     * @param field
     * @param c cross or circle
     * @return the new updated Field (including your move)
     */
    public abstract Field onTurn(Field field,int c);

    /**
     * The other players turn
     */
    public abstract void onWait();

    /**
     * someone won
     *
     * @param field
     * @param winnerUsername
     * @param winner
     */
    public abstract void onEnd(Field field, String winnerUsername,int winner,int c);

}
