package play.game;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

/**
 * contains Information about the 3*3 TicTacToe field
 *
 * @author Aaron
 */
public class Field implements Serializable {

    /*
        [x][y]
            x=0     x=1     x=2   
        y=0 []      []      [] 
        y=1 []      []      [] 
        y=2 []      []      [] 

     */
    public int[][] fieldArray = new int[3][3];

    public void set(int x, int y, int c) {
        if (c != 1 && c != 2) {
            throw new IllegalArgumentException("Only 1 or 2 are allowed");
        }
        fieldArray[x][y] = c;
    }

    /**
     *
     * @return the number of moves made
     */
    public int getMoves() {
        int numberOfZeros = (int) Arrays.stream(fieldArray)
                .flatMapToInt(i -> Arrays.stream(i))
                .filter(i -> i == 0)
                .count();
        return 9 - numberOfZeros;
    }

    /**
     *
     * @return 0 = no one wons 1 = 1 wons etc
     */
    public int getWinner() {
        //vertically
        for (int x = 0; x < 3; x++) {
            int[] col = fieldArray[x];
            int w = checkWin(col);
            if (w != 0) {
                return w;
            }
        }
        //horizontally
        for (int y = 0; y < 3; y++) {
            int[] row = new int[3];
            for (int x = 0; x < 3; x++) {
                row[x] = fieldArray[x][y];
            }
            int w = checkWin(row);
            if (w != 0) {
                return w;
            }
        }
        //diagonally
        int[] line = new int[3];

        line[0] = fieldArray[0][0];
        line[1] = fieldArray[1][1];
        line[2] = fieldArray[2][2];

        int w = checkWin(line);
        if (w != 0) {
            return w;
        }

        line[0] = fieldArray[0][2];
        line[1] = fieldArray[1][1];
        line[2] = fieldArray[2][0];

        w = checkWin(line);
        if (w != 0) {
            return w;
        }

        return 0;
    }

    private int checkWin(int[] arr) {
        if (IntStream.of(arr).distinct().count() == 1) {
            return arr[0];
        }
        return 0;
    }

    public static byte[] serialize(Field field) {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(field);
            return bos.toByteArray();
        } catch (IOException ex) {
            Logger.getLogger(Field.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Field deserialize(byte[] bytes) {
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            ObjectInputStream ois = new ObjectInputStream(bis);
            return (Field) ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Field.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
