package play.game;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import util.EncryptIO;

/**
 * An ActiveGame which has a connection to another player, server and client role aren't importent but there is a gameLeader role
 * @author Aaron
 */
public class ActiveGame {

    private final Socket CONNECTION;
    private DataOutputStream dos = null;
    private DataInputStream dis = null;

    private final String OPPONENTS_USERNAME;
    private final String USERNAME;
    private final byte[] AES_KEY;

    private GameState gameState; //creating, running, finfished
    private boolean gameLeader;

    private Field field; //the playing Field 3x3

    private int winner;
    private String winnerUsername;

    /**
     * creates an ActiveGame onto an established Connection and exchanged AES keys
     */
    public ActiveGame(final Socket CONNECTION, String OPPONENTS_USERNAME, String USERNAME, byte[] AES_KEY, boolean gameLeader) {
        this.CONNECTION = CONNECTION;
        this.OPPONENTS_USERNAME = OPPONENTS_USERNAME;
        this.USERNAME = USERNAME;
        this.gameState = GameState.creating;
        this.AES_KEY = AES_KEY;
        this.gameLeader = gameLeader;

        try {
            dos = EncryptIO.getEncryptedOutputStream(AES_KEY, CONNECTION.getOutputStream());
            dis = EncryptIO.getEncryptedInputStream(AES_KEY, CONNECTION.getInputStream());
        } catch (InvalidKeyException | NoSuchPaddingException | InvalidAlgorithmParameterException | IOException ex) {
            Logger.getLogger(ActiveGame.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * start the game
     * this code will block here -> use gameHandler to handle the game
     * @param gameHandler
     * @return false if there was an unexpected error and true if someone has won
     * false can also mean the other player disconnected
     */
    public boolean startGame(GameHandler gameHandler) {
        if (dos == null || dis == null) { //make sure 
            return false;
        }

        try {
            field = new Field(); //create a new gameField
            
            boolean starting = false;
            int c = 2; //cross or cirlce
            
            if (gameLeader) { //make out who starts the game
                Random random = new Random();
                starting = random.nextBoolean();
                dos.writeBoolean(!starting);
            } else {
                starting = dis.readBoolean();
            }

            gameState = GameState.running;
            while (gameState == GameState.running) {
                if (field.getMoves() == 0 && starting) {
                    //first move
                    c = 1;//the starting is always cross
                    field = gameHandler.onTurn(field, c);
                    byte[] fieldBytes = Field.serialize(field);
                    dos.writeInt(fieldBytes.length);
                    dos.write(fieldBytes);
                }
                gameHandler.onWait();
                byte[] fieldBytes = new byte[dis.readInt()];
                dis.read(fieldBytes);
                field = Field.deserialize(fieldBytes);

                winner = field.getWinner();
                if (winner != 0) {
                    //someone has actually won
                    end();
                } else {
                    //winer was 0 so no one won
                    field = gameHandler.onTurn(field, c);
                    fieldBytes = Field.serialize(field);
                    dos.writeInt(fieldBytes.length);
                    dos.write(fieldBytes);

                    winner = field.getWinner();
                    if (winner != 0) {
                        //someone has actually won
                        end();
                    }
                }
            }

            gameHandler.onEnd(field, winnerUsername, winner, c);
            return true;
        } catch (IOException ex) {
            Logger.getLogger(ActiveGame.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private void end() {
        gameState = GameState.finished;
        winnerUsername = winner == 1 ? USERNAME : OPPONENTS_USERNAME;
    }

    @Override
    public String toString() {
        return "ActiveGame{" + "OPPONENTS_USERNAME=" + OPPONENTS_USERNAME + ", gameState=" + gameState + ", gameLeader=" + gameLeader + '}';
    }

}
