package create;

import java.net.InetAddress;
import java.util.Objects;

/**
 * Simple class to represent a found game by GameFinder
 * @author Aaron
 */
public class PublicGameEntry implements Comparable<PublicGameEntry>{

    public InetAddress address;
    public String username;
    public long lastSeen;
    
    public PublicGameEntry(InetAddress address, String username) {
        this.address = address;
        this.username = username;
        lastSeen = System.currentTimeMillis();
    }

    @Override
    public String toString() {
        return "PublicGameEntry{" + "address=" + address + ", username=" + username + ", lastSeen=" + lastSeen + '}';
    }

    @Override
    public int compareTo(PublicGameEntry o) {
        return username.compareTo(o.username);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.address);
        hash = 97 * hash + Objects.hashCode(this.username);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PublicGameEntry other = (PublicGameEntry) obj;
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        return true;
    }
    
    
    
    
}
