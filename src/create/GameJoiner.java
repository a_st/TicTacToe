package create;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import play.game.ActiveGame;
import util.EncryptIO;

/**
 * utility class to allow joining a game with a specific IPAddress
 * @author ar
 */
public class GameJoiner {

    /**
     * joins game on address with port 
     * DOESN'T check if game is actually running on opponnents side - might throw Exception
     * @param address server address
     * @param port server port
     * @param username your username
     * @return an ActiveGame
     * @throws IOException
     */
    public static ActiveGame joinGame(InetAddress address, int port, String username) throws IOException {
        try {
            Socket con = new Socket(address, port);

            InputStream is = con.getInputStream();
            OutputStream os = con.getOutputStream();
            EncryptIO eio = new EncryptIO(is, os);
            byte[] key = eio.client();

            DataOutputStream dos = EncryptIO.getEncryptedOutputStream(key, os);
            DataInputStream dis = EncryptIO.getEncryptedInputStream(key, is);
            
            dos.writeUTF(username);
            dos.flush();
            
            String opUsername = dis.readUTF();
            return new ActiveGame(con, opUsername, username, key, false);

        } catch (NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException ex) {
            Logger.getLogger(GameJoiner.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
