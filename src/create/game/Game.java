package create.game;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import play.game.ActiveGame;
import util.EncryptIO;
import util.Observable;
import util.Settings;

/**
 * Runs on server to create a public game (with init()) or a private game
 * (without init()) uses Port specified in the server.properties (see:
 * util.Settings.java)
 *
 * @author Aaron
 */
public class Game {

    private String username; //own username
    private volatile boolean playerFound = false; //used to stop broadcaster Thread if a player is found
    private final Properties SERVER_PROPERTIES;

    /**
     * After creating call init() for public game to be found by the GameFinder
     *
     * @param username
     */
    public Game(String username) {
        this.username = username;
        SERVER_PROPERTIES = Settings.getProperties();
    }

    /**
     * Starts Broadcasting to be found by the gameFinder
     */
    public void init() {
        Thread gameBroadcaster = new Thread(() -> {
            try {
                DatagramSocket socket = new DatagramSocket();
                socket.setBroadcast(true);
                while (!playerFound) {
                    try {
                        byte[] bytes = username.getBytes("UTF-8"); //broadcast username
                        DatagramPacket packet = new DatagramPacket(bytes, bytes.length, InetAddress.getByName(SERVER_PROPERTIES.getProperty("broadcastAddress")), Integer.parseInt(SERVER_PROPERTIES.getProperty("port")) - 2);
                        socket.send(packet);
                        Thread.sleep(200);
                    } catch (IOException | InterruptedException ex) {
                        Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                socket.close();
            } catch (SocketException ex) {
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        gameBroadcaster.setDaemon(true);
        gameBroadcaster.start();
    }

    /**
     * starts an ActiveGame with you being the gameLeader
     *
     * @param observable status reports
     * @return an ActiveGame as gameLeader
     */
    public ActiveGame findAndStart(Observable observable) {
        observable.status("Starting server...");
        ServerSocket server = getServer();

        if (server == null) {
            observable.status("Starting server has failed!");
            return null;
        }
        observable.status("Finding player...");

        try {
            Socket con = server.accept();
            return handleConnection(observable, con);
        } catch (IOException ex) {
            observable.status("Finding players has failed!");
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private ServerSocket getServer() {
        try {
            int port = Integer.parseInt(SERVER_PROPERTIES.getProperty("port"));
            ServerSocket serverSocket = new ServerSocket(port);
            return serverSocket;
        } catch (IOException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    //exchange username and exchange AES key for later Encryption
    private ActiveGame handleConnection(Observable observable, Socket con) {
        try {
            observable.status("Player found");
            InputStream is = con.getInputStream();
            OutputStream os = con.getOutputStream();

            EncryptIO eio = new EncryptIO(is, os);
            byte[] key = eio.server();

            DataOutputStream oos = EncryptIO.getEncryptedOutputStream(key, os);
            DataInputStream ois = EncryptIO.getEncryptedInputStream(key, is);

            String opUsername = ois.readUTF();

            oos.writeUTF(username);
            observable.status("Connecting...");
            return new ActiveGame(con, opUsername, username, key, true);
        } catch (IOException | InvalidKeyException | InvalidAlgorithmParameterException | NoSuchPaddingException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
        observable.status("Something went wrong!");
        return null;
    }

}
