package create;

import java.util.SortedSet;

/**
 * An interface for the GameFinder class
 * helps to update dynamic Lists
 * @author Aaron
 */
public interface GameEntryHandler {
    
    public void update(SortedSet<PublicGameEntry> entries);
    
}
