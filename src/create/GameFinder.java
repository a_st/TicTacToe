package create;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import util.Settings;

/**
 * Manages finding games to join
 *
 * @author Aaron
 */
public class GameFinder {

    private SortedSet<PublicGameEntry> entries; //contains all Games found
    private boolean scanning = true; //used to stop scanning

    public GameFinder() {
        entries = new TreeSet<>();
    }

    /**
     * scan for games in local network
     * @param handler
     */
    public void scanForGames(GameEntryHandler handler) {
        Thread scanThread = new Thread(() -> {
            try {
                int port = Integer.parseInt(Settings.getProperties().getProperty("port"));
                DatagramSocket socket = new DatagramSocket(port - 2);
                System.out.println(port);
                socket.setSoTimeout(500);

                while (scanning) {
                    byte[] bytes = new byte[1024];
                    DatagramPacket packet = new DatagramPacket(bytes, bytes.length);
                    try {
                        socket.receive(packet);
                        PublicGameEntry entry = new PublicGameEntry(packet.getAddress(), new String(packet.getData(), "UTF-8")
                                .replaceAll(" ", "")); //replace all these weird characters (sorry for that bad comment future me)
                        entries.remove(entry);
                        entries.add(entry);

                    } catch (SocketTimeoutException ex) { //ignore because it will be thrown every 500ms (to make it possible to stop the scanning)
                    } catch (IOException ex) {
                        Logger.getLogger(GameFinder.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    //check if lastSeen isn't to long ago and delete it if it is over 2 seconds old
                    SortedSet<PublicGameEntry> entriesCopy = new TreeSet(entries);
                    entries.clear();
                    entries.addAll(entriesCopy.stream()
                            .filter((PublicGameEntry entry) -> {
                                return System.currentTimeMillis() - entry.lastSeen < 2000;
                            })
                            .collect(Collectors.toSet()));
                    handler.update(entries);
                }
            } catch (SocketException ex) {
                Logger.getLogger(GameFinder.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        scanThread.setDaemon(false);
        scanThread.start();
    }

    /**
     * stop the Scanning
     * might take up to 500 ms
     */
    public void stopScanning() {
        scanning = false;
    }

}
