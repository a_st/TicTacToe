package util;

import java.util.Properties;

/**
 * 
 * @author Aaron
 */
public class Settings {

    public static Properties getProperties() {
        Properties properties = new Properties();
        properties.setProperty("port", "13478");
        properties.setProperty("broadcastAddress", "255.255.255.255");
        return properties;
    }
}
