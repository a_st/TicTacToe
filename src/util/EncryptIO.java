package util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Aaron
 */
public class EncryptIO {

    private InputStream is;
    private OutputStream os;

    public EncryptIO(InputStream is, OutputStream os) {
        this.is = is;
        this.os = os;
    }

    public byte[] server() {
        try {
            KeyPair keyPair = buildKeyPair();
            PublicKey pubKey = keyPair.getPublic();
            PrivateKey privateKey = keyPair.getPrivate();

            os.write(pubKey.getEncoded());
            os.flush();

            byte[] encrypted = new byte[256];
            is.read(encrypted);
            Cipher d_cipher = Cipher.getInstance("RSA");
            d_cipher.init(Cipher.DECRYPT_MODE, privateKey);

            byte[] decrypted = d_cipher.doFinal(encrypted);

            return decrypted;
        } catch (NoSuchAlgorithmException | IOException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(EncryptIO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public byte[] client() {
        try {
            //retrieve publicKey
            byte[] encoded = new byte[2048];
            is.read(encoded);

            X509EncodedKeySpec ks = new X509EncodedKeySpec(encoded);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(ks);

            Cipher e_cipher = Cipher.getInstance("RSA");
            e_cipher.init(Cipher.ENCRYPT_MODE, pubKey);

            //generate passPhrase to encrypt AES connection
            String password = new RandomString(150).nextString();
            byte[] bytes = password.getBytes("utf-8");
            byte[] encrypted = e_cipher.doFinal(bytes);
            os.write(encrypted);
            os.flush();

            return bytes;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | IOException | InvalidKeySpecException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(EncryptIO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private static KeyPair buildKeyPair() throws NoSuchAlgorithmException {
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        final int keySize = 2048;
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(keySize, random);
        return keyPairGenerator.genKeyPair();
    }

    public static SecretKeySpec getAESKey(byte[] key) throws NoSuchAlgorithmException {
        MessageDigest sha = MessageDigest.getInstance("SHA-1");
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16); // use only first 128 bit
        return new SecretKeySpec(key, "AES");
    }

    public static IvParameterSpec getIV(byte[] key) {
        return new IvParameterSpec(Arrays.copyOf(key, 16));
    }

    public static DataInputStream getEncryptedInputStream(byte[] key, InputStream is) throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchPaddingException,EOFException {
        try {
            Cipher cipher = Cipher.getInstance("AES/CFB8/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, EncryptIO.getAESKey(key), EncryptIO.getIV(key));
            CipherInputStream cis = new CipherInputStream(is, cipher);
            return new DataInputStream(cis);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(EncryptIO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static DataOutputStream getEncryptedOutputStream(byte[] key, OutputStream os) throws InvalidKeyException, NoSuchPaddingException, InvalidAlgorithmParameterException {
        try {
            Cipher cipher = Cipher.getInstance("AES/CFB8/NoPadding");
            cipher.init(Cipher.ENCRYPT_MODE, EncryptIO.getAESKey(key), EncryptIO.getIV(key));
            CipherOutputStream cos = new CipherOutputStream(os, cipher);
            return new DataOutputStream(cos);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(EncryptIO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
