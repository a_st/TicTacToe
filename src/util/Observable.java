package util;

/**
 *
 * @author Aaron
 */
public interface Observable {
    
    public void status(String msg);
    
}
